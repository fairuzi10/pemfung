data Expr = C Float | Expr :+ Expr | Expr :- Expr
        | Expr :* Expr | Expr :/ Expr
        | V String
        | Let String Expr Expr
    deriving Show

foldExp :: (Expr -> a) -> (Expr -> a) -> (Expr -> a) -> (Expr -> a) -> (Expr -> a)
    -> (Expr -> Expr) -> (Expr -> a) -> Expr -> a

foldExp kons tambah kurang kali bagi varlet varv expr = case expr of
    (C c) -> kons (C c)
    (e1 :+ e2) -> tambah (e1 :+ e2)
    (e1 :- e2) -> kurang (e1 :- e2)
    (e1 :* e2) -> kali (e1 :* e2)
    (e1 :/ e2) -> bagi (e1 :/ e2)
    (Let var vexp exp) -> subfold (varlet (Let var vexp exp))
    (V var) -> varv (V var)
    where
        subfold = foldExp kons tambah kurang kali bagi varlet varv

-- change all variable var occurrence in expression
defaultVarlet (Let var vexp exp) = foldExp id recTambah recKurang recKali recBagi recLet subV exp
    where
        curVarlet nextExp = defaultVarlet (Let var vexp nextExp)
        recTambah (e1 :+ e2) = (curVarlet e1) :+ (curVarlet) e2
        recKurang (e1 :- e2) = (curVarlet e1) :- (curVarlet) e2
        recKali (e1 :* e2) = (curVarlet e1) :* (curVarlet) e2
        recBagi (e1 :/ e2) = (curVarlet e1) :/ (curVarlet) e2
        -- change all var occurrence in exp2 first, then var2 in exp2
        recLet (Let var2 vexp2 exp2) = defaultVarlet (Let var2 vexp2 (curVarlet exp2))
        subV (V v) = if (v == var) then vexp else (V v)

evaluate = foldExp (\(C c) -> c)
    (\(x :+ y) -> (evaluate x) + (evaluate y))
    (\(x :- y) -> (evaluate x) - (evaluate y))
    (\(x :* y) -> (evaluate x) * (evaluate y))
    (\(x :/ y) -> (evaluate x) / (evaluate y)) defaultVarlet (\_ -> 0.0)

main = do
    print $ evaluate (Let "x" (C 3) (Let "y" (C 2) (Let "z" (C 7) ((C 6 :/ V "x") :- (C 3 :* (C 2 :+ V "z"))))))
