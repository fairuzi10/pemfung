fib = 1:1:(map (\(x, y) -> x+y) (zip fib (tail fib)))
fib2 = 1:1:(zipWith (+) fib2 (tail fib2))

quickSort [] = []
quickSort (x:xs) = let
    arr1 = quickSort [i | i <- xs, i <= x]
    arr2 = quickSort [i | i <- xs, i > x]
    in arr1 ++ (x:arr2)

mergeSort [] = []
mergeSort (x:[]) = [x]
mergeSort arr = let
    n = length arr
    left = mergeSort (take (div n 2) arr)
    right = mergeSort (drop (div n 2) arr)
    in merge left right
    where
        merge [] y = y
        merge x [] = x
        merge (x:xs) (y:ys)
            | x < y = x:(merge xs (y:ys))
            | x >= y = y:(merge (x:xs) ys)

flipp f = \x y -> f y x

pythagoras = [(x, y, z) | z <- [2..], y <- [2..(z-1)], x <- [2..(y-1)], x*x + y*y == z*z]

maxx :: Ord a => a -> a -> a -> a
maxx a b c = max (max a b) c

sieve (x:xs) = x:(sieve [i | i <- xs, i `mod` x /= 0])
prime = sieve [2..]

main = do
    print $ take 10 fib
    print $ take 10 fib2
    print $ quickSort [3,1,6,20,17,23,2,5]
    print $ mergeSort [3,1,6,20,17,23,2,5]
    print $ flipp (-) 5 3
    print $ take 5 pythagoras
    print $ maxx 8 2 5
    print $ take 10 prime
    print $ fmap (+5) (Just 3)
    print $ (Just (+)) <*> (Just 3) <*> (Just 10)
