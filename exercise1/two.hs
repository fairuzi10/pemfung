one :: [Integer] -> [Integer]
one = map (+1)

two xs ys = concat $ map (\x -> map (\y -> x + y) ys ) xs
three xs = map (+ 2) $ filter (> 3) xs
four xys = map (\(x, y) -> x + 3) xys
five xys = map (\(x, y) -> x + 4) $ filter (\(x, y) -> x + y < 5) xys
six mxs = map (\x -> Just $ x + 5) mxs

one' xs = [x + 3 | x <- xs]
two' xs = [x | x <- xs, x > 7]
three' xs ys = [(x, y) | x <- xs, y <- ys]
four' xys = [x + y | (x, y) <- xys, x + y > 3]

main = do
    print $ one [1, 3, 4]
    print $ two [6, 2, 3] [3, 6, 7]
    print $ three [1, 2, 3, 4, 5, 6]
    print $ four [(1, 2), (3, 4), (5, 6)]
    print $ five [(1, 2), (3, 4), (3, 1)]
    print $ six [1, 2, 3, 6, 7]

    print $ one' [1, 3, 4]
    print $ two' [1, 2, 5, 6, 7, 9, 10]
    print $ three' [1, 2] [3, 4]
    print $ four' [(1, 2), (1, 1), (3, 7), (2, 2)]
