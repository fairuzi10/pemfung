mylength = sum . map (const 1)

mapmap xs = map (+1) (map (+1) xs)

iter :: (Eq a, Num a) => a -> (a -> a) -> a -> a
iter 0 _ x = x
iter n f x = f $ iter (n-1) f x

lambda = \n -> iter n succ

sumSquare :: (Num a, Enum a) => a -> a
sumSquare n = foldr (+) 0 $ map (^ 2) [1..n]

mystery xs = foldr (++) [] (map sing xs)
    where
    sing x = [x]

composeList :: ([(a -> a)]) -> (a -> a)
composeList = foldr (.) id

main = do
    print $ mylength [1, 3, 5, 6]
    print $ mapmap [1, 2, 3]
    print $ iter 3 (^2) 2
    print $ lambda 2 1
    print $ sumSquare 5
    print $ mystery [1,2,3]
    print $ composeList [(^ 2), (+ 1)] 9
