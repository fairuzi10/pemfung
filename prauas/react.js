const Component = () => {
    const name = useFormInput('Fairuzi')
    const surname = useFormInput('Teguh')
    const theme = useContext(ThemeContext)
    // const [name, setName] = useState('Fairuzi')
    // const [surname, setSurname] = useState('Teguh')
    return (
        <div>
            {/* <input value={name} onChange={e => setName(e.target.value)}></input>
            <input value={surname} onChange={e => setSurname(e.target.value)}></input> */}
            <input {...name}></input>
            <input {...surname}></input>
            <div>{name + ' ' + surname}</div>
        </div>
    )
}

const ThemeContext = React.useContext('theme-class')

const useFormInput = (initValue) => {
    const [value, setValue] = useState(initValue)
    return {
        value,
        onChange: e => setValue(e.target.value)
    }
}

const useDocumentTitle = title => {
    useEffect(() => {
        document.window.title = title
        return () => {
            // cleanup
        }
    })
}
