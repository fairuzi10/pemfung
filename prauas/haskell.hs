rotabc = map rotate
    where
        rotate 'a' = 'b'
        rotate 'b' = 'c'
        rotate 'c' = 'a'
        rotate x = x
        -- rotate x =
        --     case x of
        --         'a' -> 'b'
        --         'b' -> 'c'
        --         'c' -> 'a'
        --         x -> x
        -- rotate x
        --     | x == 'a' = 'b'
        --     | x == 'b' = 'c'
        --     | x == 'c' = 'a'
        --     | otherwise = x

last' = head . reverse
last'' x = foldl reducer (head x) x
            where
                reducer acc cur = cur

compose :: (Num a, Num b, Num c) => (b -> c) -> (a -> b) -> (a -> c)
compose f g = \x -> f(g x)

main = do
    print (rotabc "abcde")
    print (last' "abcde")
    print (compose (+3) (*2) 5)
    print (last'' [1,2,3,4])
