const reducer = (state, action) => {
    switch(action.type) {
        case "ADD_TODO": {
            return action.name.length > 0
                ? [...state, {
                    id: state.length > 0 ? Math.max(...state.map(todo => todo.id)) + 1 : 0,
                    name: action.name,
                    completed: false
                }]
                : state
        }
        default: {
            return state
        }
    }
}

const Component = () => {
    const [state, dispatch] = useReducer(reducer, [])
    return (
        <div>
            <button onClick={() => dispatch({type: 'ADD_TODO', name: 'UAS'})}></button>
        </div>
    )
}
